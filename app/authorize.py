#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Manuel Ramón Varela López"

from flask import session, abort
from functools import wraps
import os

if os.getenv("PERSISTENT_SYSTEM") == "db":
    import dao_db as dao
else:
    import dao_file as dao

def login(username, contrasinal):
    try:
        dao.login(username, contrasinal)
        return True
    except:
        return False

def userauth(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        if "user" not in session:
            abort(401)
        return function(*args, **kwargs)
    return wrapper

def rexistro(nome, usuario, contrasinal):
    try:
        dao.gardar_usuario(nome, usuario, contrasinal)
        return True
    except:
        return False