#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import hashlib
import json

filename_users = "./data.json"

def gardar_usuario(nome, usuario, contrasinal):
    if os.path.exists(filename_users):
        usuarios = read_file_users()
        if exist_user(usuarios, usuario):
            raise Exception
        contrasinal_hash = hash_contrasinal(contrasinal)
        usuario_dict = {"nome": nome, "usuario": usuario, "contrasinal": contrasinal_hash}
        usuarios.append(usuario_dict)
        save_file_users(usuarios)
    else:
        contrasinal_hash = hash_contrasinal(contrasinal)
        usuario_dict = {"nome": nome, "usuario": usuario, "contrasinal": contrasinal_hash}
        usuarios = []
        usuarios.append(usuario_dict)
        save_file_users(usuarios)


def login(username, contrasinal):
    if not os.path.exists(filename_users):
        raise Exception
    usuarios = read_file_users()
    for user in usuarios:
        if user["usuario"] == username:
            contrasinal_hash = hash_contrasinal(contrasinal)
            if user["contrasinal"] == contrasinal_hash:
                user.pop("contrasinal")
                return user
            else:
                raise Exception
    raise Exception


def exist_user(usuarios, username):
    for user in usuarios:
        if user["usuario"] == username:
            return True
    return False

def read_file_users():
    f = open(filename_users, "r")
    json_usuarios = f.read()
    f.close()
    return json.loads(json_usuarios)

def save_file_users(usuarios):
    json_usuarios = json.dumps(usuarios)
    f = open(filename_users, "w")
    f.write(json_usuarios)
    f.close()

def hash_contrasinal(contrasinal):
    hash = hashlib.md5(contrasinal.encode('utf-8')).hexdigest()
    return hash