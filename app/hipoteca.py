#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Manuel Ramón Varela López"

# Calcula a cota mensual dunha hipoteca a partir dos seus datos
# cantidade (float): cantidade do préstamos hipotecario
# interes_anual (int): interes anual en porcentaxe
# numero_anos (int): número de anos nos que se devolvera a préstamo
# devolve a cota mensual nun float 
def calculo_cota_mensual(cantidade, interes_anual, numero_anos):
    
    # Calculamos o numero de cotas (meses)
    n = (12 * numero_anos)
        
    # Pasamos o interes a numero entre 0 e 1 e calculamos o interes mensual
    interes_anual = interes_anual / 100
    i = interes_anual / 12
    
    # Realizamos o calculo
    parte_abaixo = 1 - ( 1 + i )**(n * -1)
    parte_arriba = (cantidade * i)
    cota_mensual = parte_arriba/ parte_abaixo

    return cota_mensual
