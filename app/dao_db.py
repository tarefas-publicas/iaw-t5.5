#!/usr/bin/env python
# -*- coding: utf-8 -*-

import hashlib
from database import Usuario, Hipoteca, db


def gardar_usuario(nome, usuario, contrasinal):
    usuarios = Usuario.query.filter_by(username=usuario)
    if usuarios.count() > 0:
        raise Exception
    contrasinal_hash = hash_contrasinal(contrasinal)
    user = Usuario()
    user.username = usuario
    user.nome = nome
    user.contrasinal = contrasinal_hash
    db.session.add(user)
    db.session.commit()


def login(username, contrasinal):
    contrasinal_hash = hash_contrasinal(contrasinal)
    existe = Usuario.query.filter_by(username=username, contrasinal=contrasinal_hash).count()
    if existe > 0:
        user = Usuario.query.filter_by(username=username, contrasinal=contrasinal_hash).first()
        return {"nome": user.nome, "usuario": user.username }
    raise Exception


def hash_contrasinal(contrasinal):
    hash = hashlib.md5(contrasinal.encode('utf-8')).hexdigest()
    return hash

def gardar_hipoteca(username, hipoteca_dict):
    usuario = Usuario.query.filter_by(username=username).first()
    print(usuario)
    hipoteca = Hipoteca(cantidade = hipoteca_dict["cantidade"], 
        interes = hipoteca_dict["interes_anual"],
        anos = hipoteca_dict["anos"],
        cota = hipoteca_dict["cota_mensual"],
        url_imaxe = hipoteca_dict["url_imaxe"],
        nome_imaxe = hipoteca_dict["nome_imaxe"],
        id_usuario = usuario.id)
    db.session.add(hipoteca)
    db.session.commit()

def select_hipotecas(username):
    usuario = Usuario.query.filter_by(username=username).first()
    hipotecas = Hipoteca.query.filter_by(id_usuario=usuario.id).all()
    lista_hipotecas = []
    for hipoteca in hipotecas:
        hip = {
            "cantidade": hipoteca.cantidade,
            "interes_anual": hipoteca.interes,
            "anos": hipoteca.anos,
            "cota_mensual": hipoteca.cota,
            "url_imaxe": hipoteca.url_imaxe,
            "nome_imaxe": hipoteca.nome_imaxe
        }
        lista_hipotecas.append(hip)
    return lista_hipotecas

def eliminar_hipotecas(username):
    usuario = Usuario.query.filter_by(username=username).first()
    hipotecas = Hipoteca.query.filter_by(id_usuario=usuario.id).all()
    for hipoteca in hipotecas:
        db.session.delete(hipoteca)
    db.session.commit()

