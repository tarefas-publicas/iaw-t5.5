from sqlalchemy import Column, String, Integer, Float, ForeignKey
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


db = SQLAlchemy()
def init_db(app):
    db.init_app(app)
    migrate = Migrate(app, db)
    return db, migrate

class Usuario(db.Model):
    """usuarios do noso modelo"""
    __tablename__ = 'usuarios'
    id = Column(Integer, primary_key=True)
    username = Column(String(100), nullable=False, unique=True)
    nome = Column(String(100), nullable=False)
    contrasinal = Column(String(100), nullable = False)
    apelidos = Column(String(100), nullable = True)

class Hipoteca(db.Model):
    """hipotecas do noso modelo"""
    __tablename__= "hipotecas"
    id = Column(Integer, primary_key=True)
    cantidade = Column(Float, nullable=False)
    interes = Column(Float, nullable=False)
    anos = Column(Integer, nullable=False)
    cota = Column(Float, nullable=False)
    nome_imaxe = Column(String(100),nullable=False)
    url_imaxe = Column(String(100),nullable=False)
    id_usuario = Column(Integer, ForeignKey('usuarios.id'))