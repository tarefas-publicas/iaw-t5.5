#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Manuel Ramón Varela López"

from flask import Flask, render_template, request, send_file, redirect, url_for, session
from hipoteca import calculo_cota_mensual
import os
import hashlib
from flask_session import Session
import authorize
from authorize import userauth
from database import init_db


if os.getenv("PERSISTENT_SYSTEM") == "db":
    import dao_db as dao
else:
    import dao_file as dao


# Creamos a aaplicación
app = Flask(__name__, template_folder='./templates')

# Configuración sesión
app.config["SESSION_PERMANENT"] = os.getenv("SESSION_PERMANENT")
app.config["PERMANENT_SESSION_LIFETIME"] = int(os.getenv("PERMANENT_SESSION_LIFETIME"))
app.config["SESSION_TYPE"] = os.getenv("SESSION_TYPE")
# Isto debe de ir sempre ao final, tras indicar os paŕametros de configuración
Session(app)

app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("SQLALCHEMY_DATABASE_URI")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db, migrate = init_db(app)


@app.route('/')
def raiz():
    if "user" in session:
        return redirect(url_for('hipoteca'))
    else:
        return redirect(url_for('login'))

@app.route('/login/', methods=["GET", "POST"])
@app.route('/login/<string:error>', methods=["GET","POST"])
def login(error=None):
    if request.method == "GET":
        return render_template("login.html",error=error)
    else:
        username = request.form.get("usuario")
        contrasinal = request.form.get("contrasinal")
        if authorize.login(username, contrasinal):
            session["user"] = username
            return redirect(url_for('raiz'))
        return redirect(url_for('login', error="error"))

@app.route('/logout/')
def logout():
    session.pop("user")
    return redirect(url_for('raiz'))

@app.route('/signin/', methods=["GET", "POST"])
@app.route('/signin/<string:error>', methods=["GET","POST"])
def rexistro(error=None):
    if request.method == "GET":
        return render_template("rexistro.html",error=error)
    else:
        nome = request.form.get("nome")
        usuario = request.form.get("usuario")
        contrasinal = request.form.get("contrasinal")
        flag = authorize.rexistro(nome, usuario, contrasinal)
        if flag:
            return redirect(url_for('login'))
        else:
            return redirect(url_for('rexistro',error="error"))

@app.route('/proba/')
def proba():
    return render_template("proba.html")

@app.route("/nome/")
@app.route("/nome/<string:nome>")
@app.route("/nome/<string:nome>/<string:apelidos>")
@userauth
def nome(nome=None,apelidos=None):
	if nome and apelidos:
		return render_template("nome.html", nome=nome, apelidos=apelidos)
	elif nome:
		return render_template("nome.html", nome=nome, apelidos="Snow")
	else:
		return render_template("nome.html", nome="Jon", apelidos="Snow")

@app.route('/hipoteca/', methods=["GET", "POST"])
@userauth
def hipoteca():
    if request.method == "GET":
        return render_template("hipoteca.html")
    else:
        
        # Collemos os valores do formulario
        cantidade = float(request.form.get("cantidade"))
        interes_anual = int(request.form.get("interes"))
        anos = int(request.form.get("anos"))

        # Collemos a imaxe
        ficheiro = request.files.get("ficheiro")
        nome_ficheiro = ficheiro.filename
        aux = os.path.splitext(nome_ficheiro)
        extension = aux[len(aux)-1]
        nome_hash = hashlib.md5(ficheiro.read()).hexdigest()
        ficheiro.seek(0)
        ficheiro.save(os.path.join("./app/static/uploads",nome_hash + extension))

        # Calculamos o numero de cotas (meses)
        cota = calculo_cota_mensual(cantidade,interes_anual,anos)

        hip = {
            "cantidade": cantidade,
            "interes_anual": interes_anual,
            "anos": anos,
            "cota_mensual": cota,
            "url_imaxe": "/uploads/img/" + nome_hash + extension,
            "nome_imaxe": nome_hash + extension
        }

        usuario = session["user"]
        dao.gardar_hipoteca(usuario, hip)


        return render_template("ver_hipoteca.html", hipoteca=hip)

@app.route('/hipotecas/ver/')
@userauth
def ver_hipotecas():
    usuario = session["user"]
    hipotecas = dao.select_hipotecas(usuario)
    return render_template("ver_hipotecas.html", hipotecas=hipotecas)

# Ruta: para eliminar as hipotecas
@app.route('/hipotecas/eliminar/', methods=["POST"])
@userauth
def eliminar_hipotecas():
    
    # Se hai hiptecas debemos borrala
    usuario = session["user"]
    hipotecas = dao.select_hipotecas(usuario)
    for hipoteca in hipotecas:
        nome = hipoteca["nome_imaxe"]
        try:
            os.remove(os.path.join("./app/static/uploads", nome))
        except Exception as e:
            print(e)
    
    # Borramos as hipotecas
    dao.eliminar_hipotecas(usuario)    

    return render_template("ver_hipotecas.html")

################## Contido estático

@app.route('/img/<string:imaxe>')
def imaxes(imaxe):
    # Construimos a ruta da imaxe
    url = './static/img/' + imaxe
    # Collemos a extensión da imaxe
    extension = os.path.splitext(imaxe)[1].replace('.', '')
    # Construimos o mimetype da imaxe
    mimetype = 'image/' + extension
    # Devolvemos a imaxe
    return send_file(url, mimetype=mimetype)

@app.route('/uploads/img/<string:imaxe>')
def uploads(imaxe):
    # Construimos a ruta da imaxe
    url = './static/uploads/' + imaxe
    # Collemos a extensión da imaxe
    extension = os.path.splitext(imaxe)[1].replace('.', '')
    # Construimos o mimetype da imaxe
    mimetype = 'image/' + extension
    # Devolvemos a imaxe
    return send_file(url, mimetype=mimetype)

@app.route('/pdf/<string:nome_pdf>')
def pdfs(nome_pdf):
    # Construimos a ruta da imaxe
    url = './static/pdf/' + nome_pdf
    # Collemos a extensión da imaxe
    extension = os.path.splitext(nome_pdf)[1].replace('.', '')
    # Construimos o mimetype da imaxe
    mimetype = 'application/' + extension
    # Devolvemos a imaxe
    return send_file(url, mimetype=mimetype)


################# Xestión de erros

@app.errorhandler(404)
def page_not_found(error):
    return render_template("404.html")

@app.errorhandler(401)
def page_not_found(error):
    return render_template("401.html")

################# Comandos CLI

@app.cli.command("crear-taboas")
def crear_taboas():
    print("Creando base de datos...")
    db.create_all()
    print("Base de datos creada")

@app.cli.command("eliminar-taboas")
def eliminar_taboas():
    print("Elimnando taboas")
    db.drop_all()
    print("Taboas eliminadas")